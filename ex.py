from bs4 import BeautifulSoup as bs

def writer(page):
	soup = bs(page, 'html.parser')
	f = open("results.txt", 'a')
	f.write("\n\n")
	for tag in soup.find_all('strong'):
		val=tag
		bufferread =[]
		for val in val.stripped_strings:
			bufferread.append(val)
		towrite ='\n'.join(bufferread)
		f.write('\n')
		f.write(towrite)
			